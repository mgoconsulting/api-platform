# **MGO API Platform Utils Bundle**

This package contains a Symfony Bundle with utils for API Platform.


## Installation

#### Installation with composer
```sh
composer config repositories.mgoconsulting/api-platform git https://bitbucket.org/mgoconsulting/api-platform.git
composer require mgoconsulting/api-platform
```
#### Enable the bundle and config setup

In *config/bundles.php*
```php
<?php
return [
    ...
    Mgo\ApiPlatform\MgoApiPlatformBundle::class => ['all' => true],
];
```

Configuration Reference in *config/packages/mgo_apiplatform.yaml*
```yaml
# Default configuration for extension with alias: "mgo_apiplatform"
mgo_config:
    providers:
        # config for grouping data providers
        grouping:
            # (optional) enable/disable grouping provider
            enabled: false
            # (optional) name of the grouping GET parameter
            grouping_parameter_name: '_grouping'
            # (optional) name of the aggregation GET parameter
            aggregation_parameter_name: '_aggregation'
            # (optional) name of the aggregation extra fields GET parameter
            extra_parameter_name: '_extra'
            # (optional) restrictions of the grouping behaviour
            restrictions:
                Mgo\Entity\Name: # restrictions for this class Mgo\Entity\Name
                    # paths available for grouping
                    paths:
                        custom_name: 'item.getField()' # expression language
                        relation_custom_name: 'item.getRelation().getId()' # expression language
                    # aggregation configuration
                    aggregation:
                        paths:
                            quantity: 'qty' # for ?_aggregation=quantity # property accessor
                            name: 'article.name' # for ?_aggregation=name # property accessor
                        extras:
                            firstname: 'person.name' # for ?_extra=person # property accessor
                    # filters
                    filters:
                        custom_filter: 'item.getLevel() < 5' # expression language
```







## Type Discriminator Filter

Add a filter for the discriminator class

```php
<?php

namespace Mgo\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Mgo\ApiPlatform\Filter\DiscriminatorFilter;

/**
 * @ApiResource()
 * @ApiFilter(DiscriminatorFilter::class)
 *
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({
 *     "car" = "Mgo\Entity\Car",
 *     "truck" = "Mgo\Entity\Truck",
 *     "motorcycle" = "Mgo\Entity\Motorcycle"
 * })
 */
abstract class AbstractVehicule
{
...
```
Will add a filter for `/vehicules?type=car`,  `/vehicule?type=truck`...





## Grouping Data Provider

Add a GET parameter to group collection results
`/vehicules?&_grouping[]=type&_grouping[]=brand`
```json
[
  "car": {
    "toyota": {
        {"id": 23},
        {"id": 25},
    },
    "opel": {
        {"id": 999},
    }
  },
  "truck": {
    "isuzu": {
        {"id": 46},
        {"id": 79},
    }
  }
]
```

To enable the grouping with no restrictions:
```yaml
# config/packages/mgo_apiplatform.yaml
mgo_apiplatform:
    providers:
        grouping: true
```

To enable the grouping with restrictions:
```yaml
# config/packages/mgo_apiplatform.yaml
mgo_apiplatform:
    providers:
        grouping:
            # list of classes
            Mgo\Entity\Car: ~
            Mgo\Entity\Truck: ~
            # or just abstract classes (will include Car and Truck)
            Mgo\Entity\AbstractVehicule: ~
            # or interfaces (will include vehicules classes with tyres)
            Mgo\Entity\TypeInterface: ~
            # restrictions on paths:
            # will allow grouping only values _grouping[]=property or _grouping[]=example.path.id
            Mgo\Entity\Example:
                paths: ['property', 'example.path.id']
```

To enable the grouping with aggregations (needs restrictions):
```yaml
# config/packages/mgo_apiplatform.yaml
mgo_apiplatform:
    providers:
        grouping:
            Mgo\Entity\Car:
                paths: ['name']
                aggregation:
                    paths:
                        quantity: 'qty'
```
`/vehicules?&_grouping[]=name&_aggregation[]=quantity`
```json
[
  "toyota": {
    "quantity": 156
  },
  "isuzu": {
    "quantity": 48
  }
]
```


To enable the grouping with aggregations with extra fields (needs restrictions):
```yaml
# config/packages/mgo_apiplatform.yaml
mgo_apiplatform:
    providers:
        grouping:
            Mgo\Entity\Car:
                paths: ['name']
                aggregation:
                    paths:
                        quantity: 'qty'
                    extras:
                        retailer: 'owner.name'
```
`/vehicules?&_grouping[]=name&_aggregation[]=quantity`
```json
[
  "toyota": {
    "quantity": 156,
    "retailer": "John Retail"
  },
  "isuzu": {
    "quantity": 48,
    "retailer": "Louis Gordano"
  }
]
```







## Security Extension

This is a ([Api Platform extension](https://api-platform.com/docs/core/extensions/)) to filter queries on items and collections
for user permissions.

```yaml
# config/packages/mgo_apiplatform.yaml
mgo_apiplatform:
    extensions:
        security:
            # to enable/disable this extension
            enabled: true
            restrictions:
                # quick restrictions example
                App\Entity\Truck: [drivers]
                # complete restrictions example
                App\Entity\Car:
                    # optional symfony security condition
                    condition: 'not is_granted("ROLE_ADMIN")'
                    # list fields that contains users
                    user_filter_fields:
                        createdBy: ~  # many to one relationship auto managed
                        owners: ~  # many to many relationship auto managed
                        drivers: # many to one relationship custom managed
                            where_dql_part: 'licencedDrivers.id = :loggedin_user' # where override
                            joins:
                                # alias: join part
                                licencedDrivers: 'drivers.licenced'
```
