<?php

namespace Mgo\ApiPlatform\DataProvider;

use ApiPlatform\Core\Bridge\Doctrine\Orm\CollectionDataProvider;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\PaginationExtension;
use ApiPlatform\Core\Exception\InvalidArgumentException;
use ApiPlatform\Core\Util\RequestParser;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\ExpressionLanguage\SyntaxError;

class GroupingProvider extends CollectionDataProvider
{
    private const DISABLED_EXTENSIONS = [
        // desactivate pagination extension to get all the results
        PaginationExtension::class
    ];

    /** @var Request */
    private $request;

    /** @var array */
    private $config;

    /** @var \Symfony\Component\PropertyAccess\PropertyAccessorInterface */
    private $pa;

    /** @var ExpressionLanguage */
    private $el;

    public function __construct(
        RequestStack $requestStack,
        ManagerRegistry $managerRegistry,
        RewindableGenerator $collectionExtensions,
        array $config
    ) {
        // since symfony/http-foundation 5.3, use getMainRequest() instead of getMasterRequest()
        if (\is_callable([$requestStack, 'getMainRequest'])) {
            $this->request = $requestStack->getMainRequest();
        } else {
            $this->request = $requestStack->getMasterRequest();
        }
        $this->config = $config['providers']['grouping'] ?? [];
        $this->pa = PropertyAccess::createPropertyAccessorBuilder()
                ->enableExceptionOnInvalidIndex()
                ->getPropertyAccessor();

        $this->el = new ExpressionLanguage();

        $extensions = [];
        foreach ($collectionExtensions as $extension) {
            // desactivate pagination extension to get all the results
            if (!\in_array(\get_class($extension), self::DISABLED_EXTENSIONS)) {
                $extensions[] = $extension;
            }
        }

        parent::__construct($managerRegistry, $extensions);
    }

    public function getCollection(string $resourceClass, string $operationName = null, array $context = [])
    {
        $restrictions = $this->getRestrictions($resourceClass);

        // check mandatory parameters
        if ($restrictions['mandatory'] ?? false) {
            $diff = \array_diff(
                $restrictions['mandatory'],
                // Dots are replaced by underscores in parameter names
                // @see https://github.com/symfony/symfony/issues/9009
                array_keys(RequestParser::parseRequestParams($this->request->server->get('QUERY_STRING')))
            );
            if ($diff) {
                $message =  sprintf(
                    'Missing mandatory parameter(s): %s',
                    \implode(', ', $diff)
                );
                throw new InvalidArgumentException($message, 404);
            }
        }

        /** @var array $results */
        $results = parent::getCollection($resourceClass, $operationName, $context);

        $collection = [];
        foreach ($results as $i => $object) {
            foreach ((array) $this->getParameters('filter_parameter_name') as $filterName) {
                // check mgo_apiplatform.providers.grouping.restrictions.XXXXXXX.filters
                if (($restrictions['filters'] ?? false) && !isset($restrictions['filters'][$filterName])) {
                    continue;
                }
                try {
                    $value = $this->el->evaluate($restrictions['filters'][$filterName], ['item' => $object]);
                    if (!$value) {
                        continue 2; // continue foreach $results
                    }
                } catch (SyntaxError $e) {
                    throw new InvalidArgumentException("Invalid filter '{$filterName}' : {$e->getMessage()}", 404);
                }
            }

            $itemPropertyPath = '';
            foreach ((array) $this->getParameters('grouping_parameter_name') as $grouping) {
                // check mgo_apiplatform.providers.grouping.restrictions.XXXXXXX.paths restrictions
                if (($restrictions['paths'] ?? false) && !isset($restrictions['paths'][$grouping])) {
                    continue;
                }

                $groupingEl = $restrictions['paths'][$grouping] ?? false;
                try {
                    $value = $this->el->evaluate($groupingEl, ['item' => $object]);
                } catch (SyntaxError $e) {
                    throw new InvalidArgumentException("Invalid grouping '{$grouping}' : {$e->getMessage()}", 404);
                }

                switch ($type = \gettype($value)) {
                    case 'boolean':
                        $value = $value ? 'TRUE' : 'FALSE';
                        break;

                    case 'NULL':
                        $value = $type;
                        break;

                    case 'object':
                        if (!\is_callable([$value, '__toString'])) {
                            $message = sprintf(
                                "Invalid grouping path '%s': Returned an object not convertible to a string",
                                $grouping
                            );
                            // 500 = config error
                            throw new InvalidArgumentException($message, 500);
                        }
                        break;
                }

                $itemPropertyPath .= sprintf('[%s]', false == $value ? "0$value" : $value);
            }

            // get next key in array
            $i = 0;
            if ($this->pa->isReadable($collection, $itemPropertyPath)) {
                $i = count($this->pa->getValue($collection, $itemPropertyPath));
            }

            // data is aggregated
            $data = $object;
            if (
                ($restrictions['aggregation']['paths'] ?? false)
                && $aggregators = $this->getParameters('aggregation_parameter_name')
            ) {
                // filter aggregators from config
                $aggregators = \array_intersect_key(
                    $restrictions['aggregation']['paths'],
                    \array_flip($aggregators)
                );

                if (0 === $i) {
                    $data = array_combine(
                        array_keys($aggregators),
                        \array_fill(0, count($aggregators), 0)
                    );
                } else {
                    $data = $this->pa->getValue($collection, $itemPropertyPath);
                }

                foreach ($aggregators as $name => $field) {
                    $fieldPath = "[{$name}]";
                    $this->pa->setValue(
                        $data,
                        $fieldPath,
                        $this->pa->getValue($data, $fieldPath) + $this->pa->getValue($object, $field)
                    );
                }

                $extras = $this->getParameters('extra_parameter_name');
                if ($extras) {
                    // filter extras from config
                    $extras = \array_intersect_key(
                        $restrictions['aggregation']['extras'],
                        \array_flip($extras)
                    );
                    foreach ($extras as $field => $propertyPath) {
                        if (!$this->pa->isReadable($object, $propertyPath)) {
                            throw new InvalidArgumentException("Extra path '{$propertyPath}' is invalid.", 400);
                        }
                        $fieldPath = "[{$field}]";
                        $this->pa->setValue(
                            $data,
                            $fieldPath,
                            $this->pa->getValue($object, $propertyPath)
                        );
                    }
                }
            } else { // data is full object
                $itemPropertyPath .= "[{$i}]";
            }

            $this->pa->setValue($collection, $itemPropertyPath, $data);
        }

        return $collection;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return ($this->config['enabled'] ?? false) // config 'mgo_apiplatform.providers.grouping.enabled' must be true
            // grouping available only for GET requests
            && 'get' === $operationName
            // grouping available for collections
            && 'collection' === ($context['operation_type'] ?? false)
            // at least one grouping parameters is defined
            && $this->getParameters('grouping_parameter_name')
            // check GET parameter exists in URL
            && false !== $this->getRestrictions($resourceClass);
    }

    private function getParameters(string $configParameterName): ?array
    {
        if (
            $this->request->query->has($this->config[$configParameterName])
            && $grouping = $this->request->query->get($this->config[$configParameterName])
        ) {
            return (array) $grouping;
        }

        return null;
    }

    private function getRestrictions(string $resourceClass): ?array
    {
        // no restrictions so everything is groupable
        if (!($this->config['restrictions'] ?? false)) {
            return []; // empty array means ok and no restrictions at all
        }

        // check restrictions match
        foreach ($this->config['restrictions'] as $className => $restrictions) {
            // resourceClass match exact name, parents or interfaces.
            $match = \in_array(
                $className,
                \array_merge(
                    [$resourceClass],
                    \class_parents($resourceClass),
                    \class_implements($resourceClass)
                )
            );
            if ($match) {
                $grouping = $this->getParameters('grouping_parameter_name');
                if (
                    ($restrictions['paths'] ?? false)
                    && $grouping
                    && !\array_intersect($grouping, array_keys($restrictions['paths']))
                ) {
                    continue;
                }

                return $restrictions;
            }
        }

        return null; // does not pass restrictions
    }
}
