<?php

declare(strict_types=1);

namespace Mgo\ApiPlatform\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\FilterInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\Common\Persistence\ManagerRegistry as Doctrine;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Doctrine\ORM\QueryBuilder;

class DiscriminatorFilter implements FilterInterface
{
    private $doctrine;

    public function __construct(Doctrine $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function getDescription(string $resourceClass): array
    {
        $metadata = $this->getClassMetadata($resourceClass);
        $name = $metadata->discriminatorColumn['name'];

        return [
            $name => [
                'property' => $name,
                'type' => $metadata->discriminatorColumn['type'],
                'required' => false,
                'strategy' => 'exact',
            ],
        ];
    }

    public function apply(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null,
        array $context = null
    ) {
        $metadata = $this->getClassMetadata($resourceClass);

        // discriminator column name
        $name = $metadata->discriminatorColumn['name'];

        // check discriminator filter value exists and is valid
        if (
            isset($context['filters'][$name])
            && isset($metadata->discriminatorMap[$type = $context['filters'][$name]])
        ) {
            $alias = $queryBuilder->getRootAliases()[0];
            $queryBuilder->andWhere($queryBuilder->expr()->isInstanceOf($alias, $metadata->discriminatorMap[$type]));
        }
    }

    private function getClassMetadata(string $resourceClass): ClassMetadataInfo
    {
        $metadata = $this->doctrine->getManagerForClass($resourceClass)->getClassMetadata($resourceClass);
        if (!isset($metadata->discriminatorColumn) || !$metadata->discriminatorColumn) {
            throw new \Exception("Class {$resourceClass} does not have a discriminator");
        }

        return $metadata;
    }
}
