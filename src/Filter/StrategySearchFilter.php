<?php

declare(strict_types=1);

namespace Mgo\ApiPlatform\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Core\Exception\InvalidArgumentException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;

class StrategySearchFilter extends SearchFilter
{
    public const STRATEGY_PARAMETER_NAME = '_strategy';
    public const EXCLUDE_PARAMETER_NAME = '_exclude';
    public const STRATEGY_NAME = 'or';

    protected $joinType = Join::LEFT_JOIN;
    private $useOr = false;
    private $excludes = [];
    private $nots = [];
    private $context = [];

    public function apply(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null,
        array $context = []
    ) {
        $this->context = $context;
        $this->useOr = self::STRATEGY_NAME === ($context['filters'][self::STRATEGY_PARAMETER_NAME] ?? false);
        $this->excludes = $context['filters'][self::EXCLUDE_PARAMETER_NAME] ?? [];
        if ($this->excludes) {
            $this->excludes = array_filter(explode(',', $this->excludes));
        }
        foreach ($context['filters'] ?? [] as $name => $filter) {
            if (
                ($filter['not'] ?? false)
                && $this->isPropertyEnabled($name, $resourceClass)
                && $this->isPropertyMapped($name, $resourceClass, true)
            ) {
                $this->nots[$name] = $filter['not'];
            }
        }
        parent::apply($queryBuilder, $queryNameGenerator, $resourceClass, $operationName, $context);
    }

    /**
     * {@inheritdoc}
     */
    protected function filterProperty(
        string $property,
        $value,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null
    ) {
        if (isset($this->nots[$property])) {
            $value = $this->nots[$property];
            $alias = $queryBuilder->getAllAliases()[0];
            if ($this->isPropertyNested($property, $resourceClass)) {
                list($alias, $property) = $this->addJoinsForNestedProperty(
                    $property,
                    $alias,
                    $queryBuilder,
                    $queryNameGenerator,
                    $resourceClass
                );
            }
            $valueParameter = $queryNameGenerator->generateParameterName($property);
            $whereMethod = $this->isAndWhereStrategy($property) ? 'andWhere' : 'orWhere';
            if (\is_array($value)) {
                $queryBuilder
                    ->$whereMethod(sprintf('%s.%s NOT IN (:%s)', $alias, $property, $valueParameter));
            } else {
                $queryBuilder
                    ->$whereMethod(sprintf('%s.%s != :%s', $alias, $property, $valueParameter));
            }
            $queryBuilder->setParameter($valueParameter, $value);

            return;
        }

        return parent::filterProperty(
            $property,
            $value,
            $queryBuilder,
            $queryNameGenerator,
            $resourceClass,
            $operationName
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function addWhereByStrategy(
        string $strategy,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $alias,
        string $field,
        $value,
        bool $caseSensitive,
        $property = null,
        $context = null
    ) {
        if ($this->isAndWhereStrategy($property)) {
            return parent::addWhereByStrategy(
                $strategy,
                $queryBuilder,
                $queryNameGenerator,
                $alias,
                $field,
                $value,
                $caseSensitive,
                $property,
                $this->context
            );
        }

        $wrapCase = $this->createWrapCase($caseSensitive);
        $valueParameter = $queryNameGenerator->generateParameterName($field);

        // phpcs:disable
        switch ($strategy) {
            case null:
            case self::STRATEGY_EXACT:
                $queryBuilder
                    ->orWhere(sprintf($wrapCase('%s.%s').' = '.$wrapCase(':%s'), $alias, $field, $valueParameter))
                    ->setParameter($valueParameter, $value);
                break;
            case self::STRATEGY_PARTIAL:
                $queryBuilder
                    ->orWhere(sprintf($wrapCase('%s.%s').' LIKE '.$wrapCase('CONCAT(\'%%\', :%s, \'%%\')'), $alias, $field, $valueParameter))
                    ->setParameter($valueParameter, $value);
                break;
            case self::STRATEGY_START:
                $queryBuilder
                    ->orWhere(sprintf($wrapCase('%s.%s').' LIKE '.$wrapCase('CONCAT(:%s, \'%%\')'), $alias, $field, $valueParameter))
                    ->setParameter($valueParameter, $value);
                break;
            case self::STRATEGY_END:
                $queryBuilder
                    ->orWhere(sprintf($wrapCase('%s.%s').' LIKE '.$wrapCase('CONCAT(\'%%\', :%s)'), $alias, $field, $valueParameter))
                    ->setParameter($valueParameter, $value);
                break;
            case self::STRATEGY_WORD_START:
                $queryBuilder
                    ->orWhere(sprintf($wrapCase('%1$s.%2$s').' LIKE '.$wrapCase('CONCAT(:%3$s, \'%%\')').' OR '.$wrapCase('%1$s.%2$s').' LIKE '.$wrapCase('CONCAT(\'%% \', :%3$s, \'%%\')'), $alias, $field, $valueParameter))
                    ->setParameter($valueParameter, $value);
                break;
            default:
                throw new InvalidArgumentException(sprintf('strategy %s does not exist.', $strategy));
        }
    }

    private function isAndWhereStrategy(string $property) {
        return !$this->useOr || in_array($property, $this->excludes);
    }
}
