<?php

declare(strict_types=1);

namespace Mgo\ApiPlatform\Extension;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry as Doctrine;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

final class SecurityExtension implements QueryCollectionExtensionInterface, QueryItemExtensionInterface
{
    private $doctrine;
    private $tokenStorage;
    private $authorizationChecker;
    private $config;

    public function __construct(
        Doctrine $doctrine,
        TokenStorageInterface $tokenStorage,
        AuthorizationCheckerInterface $checker,
        array $config
    ) {
        $this->doctrine = $doctrine;
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $checker;
        $this->config = $config;
    }

    /**
     * {@inheritdoc}
     */
    public function applyToCollection(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null
    ) {
        $this->addWhere($queryBuilder, $resourceClass);
    }

    /**
     * {@inheritdoc}
     */
    public function applyToItem(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        array $identifiers,
        string $operationName = null,
        array $context = []
    ) {
        $this->addWhere($queryBuilder, $resourceClass);
    }

    private function addWhere(QueryBuilder $queryBuilder, string $resourceClass): void
    {
        $user = $this->tokenStorage->getToken()->getUser();
        if ($user && true === ($this->config['extensions']['security']['enabled'] ?? false)) {
            if ($this->config['extensions']['security']['restrictions'][$resourceClass] ?? false) {
                $restriction = $this->config['extensions']['security']['restrictions'][$resourceClass];

                // check security condition first
                if ($restriction['condition']) {
                    $el = new ExpressionLanguage();
                    $el->register('is_granted', function ($attributes, $object = 'null') {
                        return sprintf('$auth_checker->isGranted(%s, %s)', $attributes, $object);
                    }, function (array $variables, $attributes, $object = null) {
                        return $this->authorizationChecker->isGranted($attributes, $object);
                    });
                    // condition does not pass so we stop here
                    if (!$el->evaluate($restriction['condition'], ['user' => $user])) {
                        return;
                    }
                }

                // add user fields
                if ($restriction['user_filter_fields']) {
                    // alias of the main query
                    $rootAlias = $queryBuilder->getRootAliases()[0];
                    /** @var ClassMetadata $metadata */
                    $metadata = $this->doctrine->getManagerForClass($resourceClass)
                        ->getMetadataFactory()
                        ->getMetadataFor($resourceClass);
                    $queryBuilder->setParameter('loggedin_user', $user);
                    $whereDqlParts = [];
                    foreach ($restriction['user_filter_fields'] as $field => $fieldConfig) {
                        if (!$metadata->hasAssociation($field)) {
                            $message = \sprintf(
                                'SecurityExtension error: Association field %s::$%s was not found',
                                $resourceClass,
                                $field
                            );
                            throw new \RuntimeException($message);
                        }
                        $assoc = $metadata->getAssociationMapping($field);
                        // phpcs:disable
                        switch ($assoc['type']) {
                            case ClassMetadataInfo::ONE_TO_MANY:
                            case ClassMetadataInfo::MANY_TO_MANY:
                                $identifier = $assoc['joinTable']['inverseJoinColumns'][0]['referencedColumnName'] ?? 'id';
                                $queryBuilder->leftJoin("{$rootAlias}.{$field}", $field);
                                $whereDqlParts[] = $fieldConfig['where_dql_part'] ?: "{$field}.{$identifier} = :loggedin_user";
                                break;
                            default:
                                $whereDqlParts[] = $fieldConfig['where_dql_part'] ?: "{$rootAlias}.{$field} = :loggedin_user";
                                break;
                        }
                        // phpcs:enable

                        // custom joins defined in config
                        if ($fieldConfig['joins'] ?? false) {
                            foreach ($fieldConfig['joins'] as $joinAlias => $join) {
                                $queryBuilder->leftJoin($join, $joinAlias);
                            }
                        }
                    }

                    $queryBuilder->andWhere((implode(' OR ', $whereDqlParts)));
                }
            }
        }
    }
}
