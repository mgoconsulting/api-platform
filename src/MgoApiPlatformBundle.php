<?php

namespace Mgo\ApiPlatform;

use Mgo\ApiPlatform\DependencyInjection\MgoApiPlatformExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Api Platform Bundle.
 */
class MgoApiPlatformBundle extends Bundle
{
    public function getContainerExtension()
    {
        if (null === $this->extension) {
            $this->extension = $this->createContainerExtension();
        }

        return $this->extension;
    }

    protected function getContainerExtensionClass()
    {
        return MgoApiPlatformExtension::class;
    }
}
