<?php

declare(strict_types=1);

namespace Mgo\ApiPlatform\Controller;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Paginator;
use ApiPlatform\Core\Bridge\Symfony\Validator\Exception\ValidationException;
use ApiPlatform\Core\Problem\Serializer\ConstraintViolationListNormalizer;
use Doctrine\Inflector\InflectorFactory;
use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\Paginator as DoctrinePaginator;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class AbstractController
{
    /** @var bool */
    protected $transactional = true;

    /* @var RouterInterface */
    protected $router;

    /** @var ManagerRegistry */
    protected $doctrine;

    /** @var ValidatorInterface */
    protected $validator;

    /* @var TokenStorage */
    protected $tokenStorage;

    /** @var string */
    protected $pageParameterName;

    /** @var string */
    protected $itemsPerPageParameterName;

    /** @var int */
    protected $itemsPerPageDefault;

    /** @var int */
    protected $maxItemsPerPageDefault;

    /* @var \Doctrine\Inflector\Inflector */
    protected $inflector;

    /** @var \Symfony\Component\PropertyAccess\PropertyAccessorInterface */
    protected $pa;

    /* @var Request */
    protected $request;

    /* @var object */
    protected $user;

    /**
     * Route to redirect to if user not logged-in
     */
    abstract protected function getRedirectRoute(): string;

    public function __construct(
        RouterInterface $router,
        ManagerRegistry $doctrine,
        ValidatorInterface $validator,
        TokenStorageInterface $tokenStorage,
        string $pageParameterName,
        string $itemsPerPageParameterName,
        int $itemsPerPageDefault,
        int $maxItemsPerPageDefault
    ) {
        // useful args
        $this->router = $router;
        $this->doctrine = $doctrine;
        $this->validator = $validator;
        $this->tokenStorage = $tokenStorage;
        // pagination
        $this->pageParameterName = $pageParameterName;
        $this->itemsPerPageParameterName = $itemsPerPageParameterName;
        $this->itemsPerPageDefault = $itemsPerPageDefault;
        $this->maxItemsPerPageDefault = $maxItemsPerPageDefault;
        // other cool stuff
        $this->inflector = InflectorFactory::create()->build();
        $this->pa = PropertyAccess::createPropertyAccessorBuilder()
                ->enableExceptionOnInvalidIndex()
                ->getPropertyAccessor();
    }

    public function __invoke(Request $request)
    {
        // check user is logged in
        if (
            !$this->tokenStorage->getToken()
            ||
            !\is_object($this->user = $this->tokenStorage->getToken()->getUser())
        ) {
            return new RedirectResponse($this->router->generate($this->getRedirectRoute()));
        }

        // check we are on a api-platform context
        if (!$request->attributes->has('_api_normalization_context')) {
            throw new \RuntimeException('ApiPlaform attribute "_api_normalization_context" not found.');
        }

        // create method name from context
        $apiContext = $request->attributes->get('_api_normalization_context');
        if ('collection' === $apiContext['operation_type']) {
            $method = $apiContext['collection_operation_name'];
        } else {
            $method = $apiContext['item_operation_name'];
        }
        $method = $this->inflector->camelize("do" . ucfirst($method));

        if (!\method_exists($this, $method)) {
            $message =  sprintf(
                'Controller method %s::%s() is not implemented.',
                get_class($this),
                $method
            );
            throw new \RuntimeException($message);
        }

        $data = $request->attributes->get('data');

        /** @var \Doctrine\ORM\EntityManagerInterface $em */
        $em = $this->doctrine->getManager();
        if ($this->transactional) {
            $em->beginTransaction();
        }
        try {
            $this->request = $request;
            $response = $this->$method($data);
            if ($this->transactional) {
                $em->commit();
            }
            return $response;
        } catch (\Exception $e) {
            if ($this->transactional) {
                $em->rollback();
            }
            // return validation violations
            if ($e instanceof ValidationException) {
                $normalizer = new ConstraintViolationListNormalizer();

                return $this->buildExceptionJsonResponse(
                    $normalizer->normalize($e->getConstraintViolationList()),
                    400
                );
            }

            $exceptionMapping = $this->exceptionMapping();
            $exceptionClass = get_class($e);
            $code = $e->getCode();
            if ($exceptionMapping[$exceptionClass] ?? false) {
                $code = (int) $exceptionMapping[$exceptionClass];
            }

            return $this->buildExceptionJsonResponse(
                ['detail' => $e->getMessage()],
                $code
            );
        }
    }

    protected function validate(object $entity): array
    {
        $errors = [];
        foreach ($this->validator->validate($entity) as $error) {
            $errors[$error->getPropertyPath()][] = $error->getMessage();
        }

        return $errors;
    }

    protected function paginateQuery(Query $query)
    {
        $page = (int) $this->request->query->get($this->pageParameterName, 1);
        $itemsPerPage = (int) $this->request->query->get(
            $this->itemsPerPageParameterName,
            $this->itemsPerPageDefault
        );

        if ($itemsPerPage > $this->maxItemsPerPageDefault) {
            $itemsPerPage = $this->itemsPerPageDefault;
        }

        $query
            ->setFirstResult(($page - 1) * $itemsPerPage)
            ->setMaxResults($itemsPerPage);

        $doctrinePaginator = new DoctrinePaginator($query);

        return new Paginator($doctrinePaginator);
    }

    protected function buildExceptionJsonResponse($error, int $code = 500): JsonResponse
    {
        if ($error instanceof \Exception) {
            $data = ['error' => $error->getMessage()];
            $code = $error->getCode();
        } else {
            $data = $error;
        }

        return new JsonResponse($data, $this->prepareResponseCode($code));
    }

    protected function prepareResponseCode(int $code): int
    {
        // @see Response::isInvalid()
        return ($code < 100 || $code >= 600) ? 500 : $code;
    }

    protected function exceptionMapping(): array
    {
        // override this method at your convenience
        // ex: [\My\Exception => 404]
        return [];
    }
}
