<?php

namespace Mgo\ApiPlatform\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('mgo_apiplatform');
        // config component version
        if (\method_exists($treeBuilder, 'getRootNode')) {
            $root = $treeBuilder->getRootNode();
        } else {
            $root = $treeBuilder->root('mgo_config');
        }

        /* @var \Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition $root */
        $root
            ->children()
                ->arrayNode('extensions')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->booleanNode('_enabled')
                            ->defaultFalse()
                        ->end()
                        ->arrayNode('security')
                            ->canBeEnabled()
                            ->children()
                                ->arrayNode('restrictions')
                                    ->arrayPrototype()
                                        ->beforeNormalization()
                                            ->ifString()
                                            ->then(function ($value) {
                                                return [
                                                    'condition' => null,
                                                    'user_filter_fields' => $value,
                                                ];
                                            })
                                        ->end()
                                        ->children()
                                            ->scalarNode('condition')
                                                ->defaultNull()
                                            ->end()
                                            ->arrayNode('user_filter_fields')
                                                ->arrayPrototype()
                                                    ->children()
                                                        ->scalarNode('where_dql_part')
                                                            ->defaultNull()
                                                        ->end()
                                                        ->arrayNode('joins')
                                                            ->scalarPrototype()
                                                                ->isRequired()
                                                                ->cannotBeEmpty()
                                                            ->end()
                                                        ->end()
                                                    ->end()
                                                ->end()
                                            ->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                    ->validate()
                        ->ifTrue(function (array $extensions) {
                            // validate extension classes in restrictions
                            foreach (($extensions['security']['restrictions'] ?? []) as $class => $restrictions) {
                                // validate class name
                                if (!\class_exists($class) && !interface_exists($class)) {
                                    throw new \RuntimeException("Class '{$class}' does not exist.");
                                }
                            }

                            // check if one is enabled to set the global extensions enabled boolean
                            foreach ($extensions as $extension) {
                                if ($extension['enabled'] ?? false) {
                                    return true;
                                }
                            }

                            return false;
                        })
                        ->then(function ($conf) {
                            $conf['_enabled'] = true;

                            return $conf;
                        })
                    ->end()
                ->end()
                ->arrayNode('providers')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->booleanNode('_enabled')
                            ->defaultFalse()
                        ->end()
                        ->arrayNode('grouping')
                            ->canBeEnabled()
                            ->children()
                                ->scalarNode('grouping_parameter_name')
                                    ->cannotBeEmpty()
                                    ->defaultValue('_grouping')
                                ->end()
                                ->scalarNode('aggregation_parameter_name')
                                    ->cannotBeEmpty()
                                    ->defaultValue('_aggregation')
                                ->end()
                                ->scalarNode('extra_parameter_name')
                                    ->cannotBeEmpty()
                                    ->defaultValue('_extra')
                                ->end()
                                ->scalarNode('filter_parameter_name')
                                    ->cannotBeEmpty()
                                    ->defaultValue('_filter')
                                ->end()
                                ->arrayNode('restrictions')
                                    ->arrayPrototype()
                                        ->children()
                                            ->arrayNode('mandatory')
                                                ->scalarPrototype()->end()
                                            ->end()
                                            ->arrayNode('paths')
                                                ->scalarPrototype()->end()
                                            ->end()
                                            ->arrayNode('aggregation')
                                                ->children()
                                                    ->arrayNode('paths')
                                                        ->scalarPrototype()->end()
                                                    ->end()
                                                    ->arrayNode('extras')
                                                        ->scalarPrototype()->end()
                                                    ->end()
                                                ->end()
                                            ->end()
                                            ->arrayNode('filters')
                                                ->scalarPrototype()->end()
                                            ->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                    ->validate()
                        ->ifTrue(function (array $providers) {
                            // validate provider classes and properties in restrictions
                            foreach (($providers['grouping']['restrictions'] ?? []) as $class => $restrictions) {
                                // validate class name
                                if (!\class_exists($class) && !interface_exists($class)) {
                                    throw new \RuntimeException("Class '{$class}' does not exist.");
                                }
                            }

                            // check if one is enabled to set the global providers enabled boolean
                            foreach ($providers as $provider) {
                                if ($provider['enabled'] ?? false) {
                                    return true;
                                }
                            }

                            return false;
                        })
                        ->then(function ($conf) {
                            $conf['_enabled'] = true;

                            return $conf;
                        })
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
