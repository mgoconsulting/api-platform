<?php

namespace Mgo\ApiPlatform\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * Mgo ApiPlatform Extension.
 */
class MgoApiPlatformExtension extends Extension
{
    public function getAlias()
    {
        return 'mgo_apiplatform';
    }

    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('filters.yml');
        $loader->load('controllers.yml');

        // providers
        if ($config['providers']['_enabled'] ?? false) {
            $loader->load('data_providers.yml');
        }
        unset($config['providers']['_enabled']);

        // extensions
        if ($config['extensions']['_enabled'] ?? false) {
            $loader->load('extensions.yml');
        }
        unset($config['providers']['_enabled']);

        // set config in parameters
        $container->setParameter('mgo_apiplatform.configuration', $config);
    }
}
