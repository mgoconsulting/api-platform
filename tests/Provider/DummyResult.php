<?php

declare(strict_types=1);

namespace Test\Provider;

class DummyResult
{
    private $name;
    private $number;
    private float $float = 0.0;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNumber(): int
    {
        return $this->number;
    }

    public function setNumber(int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getFloat(): float
    {
        return $this->float;
    }

    public function setFloat(float $float): self
    {
        $this->float = $float;

        return $this;
    }

    public function getNameAndNumber(): string
    {
        return "Name: {$this->name} Number: {$this->number}";
    }

    public function getNumberPlus10(): int
    {
        return $this->number + 10;
    }

    public function getUniqueGrouping(): string
    {
        return "unique";
    }

    public function getSelf(): self
    {
        // juste to test exception Grouping returned an object not convertible to a string
        return $this;
    }
}
