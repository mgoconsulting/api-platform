<?php

declare(strict_types=1);

namespace Test\Provider;

use PHPUnit\Framework\TestCase;
use Mgo\ApiPlatform\DataProvider\GroupingProvider;
use Symfony\Component\HttpFoundation;
use Doctrine\Common\Persistence as Doctrine;
use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;

class GroupingProviderTest extends TestCase
{
    private static $dataCache = [];
    private static $dataCacheWithZero = [];
    private static $dataCacheWithAllAtZero = [];

    public function supportsData(): array
    {
        return [
            'nothing match at all' => [false],
            'config enabled' => [false, ['enabled' => false]],
            'config enabled and operation name' => [
                false,
                [],
                'get'
            ],
            'config enabled, operation name and operation type collection' => [
                true,
                [],
                'get',
                ['operation_type' => 'collection'],
            ],
        ];
    }

    /**
     * @dataProvider supportsData
     */
    public function testSupports(
        bool $expected,
        array $config = [],
        string $operationName = 'DummyOperation',
        array $context = []
    ): void {
        $this->assertEquals(
            $expected,
            $this->buildGroupingProvider($config)->supports(DummyResult::class, $operationName, $context)
        );
    }

    public function getCollectionDataException(): array
    {
        return [
            'Invalid grouping path' => [
                "Invalid grouping 'invalid' : Unexpected token",
                ['invalid'],
            ],
            'Grouping returned an object not convertible to a string' => [
                "Invalid grouping path 'self_invalid': Returned an object not convertible to a string",
                ['self_invalid'],
            ],
            'Invalid extra path' => [
                "Extra path 'pathDoesNotExists' is invalid.",
                ['name'],
                ['number'],
                ['failure']
            ],
            'Invalid filter' => [
                "Extra path 'pathDoesNotExists' is invalid.",
                ['name'],
                ['number'],
                ['failure']
            ],
        ];
    }

    /**
     * @dataProvider getCollectionDataException
     */
    public function testGetCollectionException(
        string $expectedExpception,
        array $groupingParameterValues = ['number'],
        array $aggregationParameterValues = [],
        array $extraParameterValues = []
    ): void {
        $this->expectExceptionMessage($expectedExpception);
        $provider = $this->buildGroupingProvider(
            [],
            $groupingParameterValues,
            $aggregationParameterValues,
            $extraParameterValues
        );
        $provider->getCollection(
            DummyResult::class,
            'get',
            ['operation_type' => 'collection']
        );
    }

    public function getCollectionSuccessData(): array
    {
        $results = $this->getResultsData();

        return [
            // grouping tests
            'grouping by number' => [
                [
                    1 => [$results['dummy1'], $results['dummy2']],
                    2 => [$results['dummy3']],
                ],
                ['number'],
            ],
            'grouping by name' => [
                [
                    'dummy 1.1' => [$results['dummy1']],
                    'dummy 1.2' => [$results['dummy2']],
                    'dummy 2.1' => [$results['dummy3']],
                ],
                ['name'],
            ],
            'grouping by number and name' => [
                [
                    1 => [
                        'dummy 1.1' => [$results['dummy1']],
                        'dummy 1.2' => [$results['dummy2']],
                    ],
                    2 => [
                        'dummy 2.1' => [$results['dummy3']],
                    ],
                ],
                ['number', 'name'],
            ],
            // aggregation tests
            'grouping by name, aggregation by number' => [
                [
                    'dummy 1.1' => ['number' => 1],
                    'dummy 1.2' => ['number' => 1],
                    'dummy 2.1' => ['number' => 2],
                ],
                ['name'],
                ['number'],
            ],
            'grouping by name, aggregation by number and added_10' => [
                [
                    'dummy 1.1' => ['number' => 1, 'added_10' => 11],
                    'dummy 1.2' => ['number' => 1, 'added_10' => 11],
                    'dummy 2.1' => ['number' => 2, 'added_10' => 12],
                ],
                ['name'],
                ['added_10', 'number'],
            ],
            'grouping by unique, aggregation number' => [
                ['unique' => ['number' => 4]],
                ['uniqueGrouping'],
                ['number'],
            ],
            'grouping by unique, aggregation added_10' => [
                ['unique' => ['added_10' => 34]],
                ['uniqueGrouping'],
                ['added_10'],
            ],
            'grouping by unique, aggregation number and added_10' => [
                ['unique' => ['number' => 4, 'added_10' => 34]],
                ['uniqueGrouping'],
                ['number', 'added_10'],
            ],
            // extra tests
            'grouping by unique, aggregation number, extra name will return the last element' => [
                ['unique' => ['number' => 4, 'name' => 'dummy 2.1']],
                ['uniqueGrouping'],
                ['number'],
                ['name'],
            ],
            'grouping by name, aggregation by number, extra name_number' => [
                [
                    'dummy 1.1' => ['number' => 1, 'name_number' => 'Name: dummy 1.1 Number: 1'],
                    'dummy 1.2' => ['number' => 1, 'name_number' => 'Name: dummy 1.2 Number: 1'],
                    'dummy 2.1' => ['number' => 2, 'name_number' => 'Name: dummy 2.1 Number: 2'],
                ],
                ['name'],
                ['number'],
                ['name_number'],
            ],
            // concat tests
            'concat grouping by name and number' => [
                [
                    'dummy 1.1 + 1' => ['number' => 1],
                    'dummy 1.2 + 1' => ['number' => 1],
                    'dummy 2.1 + 2' => ['number' => 2],
                ],
                ['concatGrouping'],
                ['number'],
            ],
            // filters tests
            'grouping by number and filtered by greater_than_11' => [
                [
                    2 => [$results['dummy3']],
                ],
                ['number'],
                [],
                [],
                ['greater_than_11']
            ],
        ];
    }

    /**
     * @dataProvider getCollectionDataWithZero
     */
    public function testGetCollectionWithZero(
        array $expected,
        string $expectedJson,
        array $resultsData,
        array $groupingParameterValues,
        array $aggregationParameterValues = [],
        array $extraParameterValues = [],
        array $filterParameterValues = []
    ): void {
        $provider = $this->buildGroupingProvider(
            [],
            $groupingParameterValues,
            $aggregationParameterValues,
            $extraParameterValues,
            $filterParameterValues,
            $resultsData
        );
        $info = $provider->getCollection(
            DummyResult::class,
            'get',
            ['operation_type' => 'collection']
        );
        $this->assertEquals($expected, $info);
        $this->assertEquals($expectedJson, json_encode($info), 'JSON data');
    }

    public function getCollectionDataWithZero(): array
    {
        $results = $this->getResultsDataWithZero();

        return [
            // grouping tests
            'grouping by number' => [
                [
                    '2' => [$results['dummy1']],
                    '00' => [$results['dummy2'], $results['dummy3']],
                ],
                '{"2":[{}],"00":[{},{}]}',
                $results,
                ['float'],
            ],
            'grouping by number and extra name' => [
                [
                    '00' => ['number' => 4],
                ],
                '{"00":{"number":4}}',
                $this->getResultsDataWithAllAtZero(),
                ['float'],
                ['number'],
                [],
            ],
        ];
    }

    /**
     * @dataProvider getCollectionSuccessData
     */
    public function testGetCollectionSuccess(
        array $expected,
        array $groupingParameterValues,
        array $aggregationParameterValues = [],
        array $extraParameterValues = [],
        array $filterParameterValues = []
    ): void {
        $provider = $this->buildGroupingProvider(
            [],
            $groupingParameterValues,
            $aggregationParameterValues,
            $extraParameterValues,
            $filterParameterValues
        );
        $this->assertEquals(
            $expected,
            $provider->getCollection(
                DummyResult::class,
                'get',
                ['operation_type' => 'collection']
            )
        );
    }

    private function buildGroupingProvider(
        array $config = [],
        array $groupingParameterValues = ['number'],
        array $aggregationParameterValues = [],
        array $extraParameterValues = [],
        array $filterParameterValues = [],
        ?array $resultsData = null
    ): GroupingProvider {
        $resultsData = $resultsData ?: $this->getResultsData();
        $groupingParameterName = $config['grouping_parameter_name'] ?? 'test_grouping';
        $aggregationParameterName = $config['aggregation_parameter_name'] ?? 'test_aggregation';
        $extraParameterName = $config['extra_parameter_name'] ?? 'test_extra';
        $filterParameterName = $config['filter_parameter_name'] ?? 'test_filter';

        // build requestStack
        $requestStack = new HttpFoundation\RequestStack();
        $request = new HttpFoundation\Request();
        if ($groupingParameterName) {
            $request->query->set($groupingParameterName, $groupingParameterValues);
        }
        if ($aggregationParameterValues) {
            $request->query->set($aggregationParameterName, $aggregationParameterValues);
        }
        if ($extraParameterValues) {
            $request->query->set($extraParameterName, $extraParameterValues);
        }
        if ($filterParameterValues) {
            $request->query->set($filterParameterName, $filterParameterValues);
        }

        $requestStack->push($request);

        // build $managerRegistry Mock
        $queryMock = $this->getMockBuilder(\Doctrine\ORM\AbstractQuery::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getResult'])
            ->getMockForAbstractClass();
        $queryMock->method('getResult')->willReturn($resultsData);
        $queryBuilderMock = $this->createMock(\Doctrine\ORM\QueryBuilder::class);
        $queryBuilderMock->method('getQuery')->willReturn($queryMock);
        $repositoryMock = $this->createMock(\Doctrine\ORM\EntityRepository::class);
        $repositoryMock->method('createQueryBuilder')->willReturn($queryBuilderMock);
        $objectManagerMock = $this->createMock(Doctrine\ObjectManager::class);
        $objectManagerMock->method('getRepository')->willReturn($repositoryMock);
        $managerRegistryMock = $this->createMock(Doctrine\ManagerRegistry::class);
        $managerRegistryMock->method('getManagerForClass')->willReturn($objectManagerMock);

        // build rewindableGenerator Mock
        $rewindableGeneratorMock = $this->createMock(RewindableGenerator::class);

        $provider = new GroupingProvider(
            $requestStack,
            $managerRegistryMock,
            $rewindableGeneratorMock,
            [
                'providers' => [
                    'grouping' => $config + [
                        'enabled' => true,
                        'grouping_parameter_name' => $groupingParameterName,
                        'aggregation_parameter_name' => $aggregationParameterName,
                        'extra_parameter_name' => $extraParameterName,
                        'filter_parameter_name' => $filterParameterName,
                        'restrictions' => [
                            DummyResult::class => [
                                'mandatory' => [],
                                'paths' => [
                                    'name' => 'item.getName()',
                                    'number' => 'item.getNumber()',
                                    'float' => 'item.getFloat()',
                                    'uniqueGrouping' => 'item.getUniqueGrouping()',
                                    'concatGrouping' => 'item.getName() ~ " + " ~ item.getNumber()',
                                    'self_invalid' => 'item.getSelf()',
                                ],
                                'aggregation' => [
                                    'paths' => [
                                        'dummy_name' => 'name',
                                        'number' => 'number',
                                        'float' => 'float',
                                        'added_10' => 'numberPlus10',
                                    ],
                                    'extras' => [
                                        'failure' => 'pathDoesNotExists',
                                        'name_number' => 'self.nameAndNumber',
                                        'name' => 'name',
                                    ],
                                ],
                                'filters' => [
                                    'greater_than_11' => 'item.getNumberPlus10() > 11',
                                ],
                            ]
                        ],
                    ]
                ],
            ]
        );
        $this->assertInstanceOf(GroupingProvider::class, $provider);

        return $provider;
    }

    private function getResultsData(): array
    {
        if (!self::$dataCache) {
            $dummy1 = new DummyResult();
            $dummy1->setName('dummy 1.1');
            $dummy1->setNumber(1);

            $dummy2 = new DummyResult();
            $dummy2->setName('dummy 1.2');
            $dummy2->setNumber(1);

            $dummy3 = new DummyResult();
            $dummy3->setName('dummy 2.1');
            $dummy3->setNumber(2);

            self::$dataCache = [
                'dummy1' => $dummy1,
                'dummy2' => $dummy2,
                'dummy3' => $dummy3,
            ];
        }

        return self::$dataCache;
    }

    private function getResultsDataWithZero(): array
    {
        if (!self::$dataCacheWithZero) {
            $dummy1 = new DummyResult();
            $dummy1->setName('dummy 1.1');
            $dummy1->setNumber(1);
            $dummy1->setFloat(2.0);

            $dummy2 = new DummyResult();
            $dummy2->setName('dummy 1.2');
            $dummy2->setNumber(1);
            $dummy2->setFloat(0.0);

            $dummy3 = new DummyResult();
            $dummy3->setName('dummy 2.1');
            $dummy3->setNumber(2);
            $dummy3->setFloat(0.0);

            self::$dataCacheWithZero = [
                'dummy1' => $dummy1,
                'dummy2' => $dummy2,
                'dummy3' => $dummy3,
            ];
        }

        return self::$dataCacheWithZero;
    }

    private function getResultsDataWithAllAtZero(): array
    {
        if (!self::$dataCacheWithAllAtZero) {
            $dummy1 = new DummyResult();
            $dummy1->setName('dummy 1.1');
            $dummy1->setNumber(1);
            $dummy1->setFloat(0.0);

            $dummy2 = new DummyResult();
            $dummy2->setName('dummy 1.2');
            $dummy2->setNumber(1);
            $dummy2->setFloat(0.0);

            $dummy3 = new DummyResult();
            $dummy3->setName('dummy 2.1');
            $dummy3->setNumber(2);
            $dummy3->setFloat(0.0);

            self::$dataCacheWithAllAtZero = [
                'dummy1' => $dummy1,
                'dummy2' => $dummy2,
                'dummy3' => $dummy3,
            ];
        }

        return self::$dataCacheWithAllAtZero;
    }
}
