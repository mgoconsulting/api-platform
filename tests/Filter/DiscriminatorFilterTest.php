<?php

declare(strict_types=1);

namespace Test\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use Mgo\ApiPlatform\Filter\DiscriminatorFilter;
use PHPUnit\Framework\TestCase;

class DiscriminatorFilterTest extends TestCase
{
    public const CLASS_NAME = 'Entity\Class';

    public function testFailDescriptionClassNoDiscriminator(): void
    {
        $this->expectExceptionMessage('Class Entity\Class does not have a discriminator');
        $filterMock = $this->createFilterWithMocks(false);
        $filterMock->getDescription(self::CLASS_NAME);
    }

    public function testSuccessDescription(): void
    {
        $filterMock = $this->createFilterWithMocks();
        $description = $filterMock->getDescription(self::CLASS_NAME);
        $this->assertEquals(
            [
                'discriminator' => [
                    'property' => 'discriminator',
                    'type' => 'string',
                    'required' => false,
                    'strategy' => 'exact',
                ],
            ],
            $description
        );
    }

    public function testFailApplyClassNoDiscriminator(): void
    {
        $this->expectExceptionMessage('Class Entity\Class does not have a discriminator');
        $filterMock = $this->createFilterWithMocks(false);

        // query builder select to test Dql later
        $queryBuilder = new QueryBuilder($this->createMock(EntityManagerInterface::class));
        $queryBuilder->select('t')->from(self::CLASS_NAME, 't');

        // query name generator mock
        $queryNameGeneratorMock = $this->createMock(QueryNameGeneratorInterface::class);

        $filterMock->apply($queryBuilder, $queryNameGeneratorMock, self::CLASS_NAME, 'get', []);
    }

    public function applyData(): array
    {
        return [
            'no filter defined in GET parameters (/api)' => [
                'SELECT t FROM Entity\Class t', // expected DQL
            ],
            'no discriminator filter defined in GET parameters (/api?whatever=value)' => [
                'SELECT t FROM Entity\Class t',
                ['whatever' => 'value'],
            ],
            'discriminator filter defined but wrong value (/api?discriminator=blabla)' => [
                'SELECT t FROM Entity\Class t',
                ['discriminator' => 'blabla'],
            ],
            'discriminator filter defined but UPPERCASED (/api?discriminator=TEST1)' => [
                'SELECT t FROM Entity\Class t',
                ['discriminator' => 'TEST1'],
            ],
            'discriminator filter test1 OK (/api?discriminator=test1)' => [
                'SELECT t FROM Entity\Class t WHERE t.discriminator INSTANCE OF Entity\Class\Test1',
                ['discriminator' => 'test1'],
            ],
            'discriminator filter test1 OK (/api?discriminator=test2)' => [
                'SELECT t FROM Entity\Class t WHERE t.discriminator INSTANCE OF Entity\Class\Test2',
                ['discriminator' => 'test2'],
            ],
        ];
    }

    /**
     * @dataProvider applyData
     */
    public function testSuccessApply(string $expectedDQL, array $filters = []): void
    {
        $filterMock = $this->createFilterWithMocks();

        // em mock for query builder
        $exprMock = $this->createMock(Expr::class);
        $exprMock->method('isInstanceOf')->willReturnCallback(function ($alias, $className) {
            return \sprintf('%s.discriminator INSTANCE OF %s', $alias, $className);
        });
        $emMock = $this->createMock(EntityManagerInterface::class);
        $emMock->method('getExpressionBuilder')->willReturn($exprMock);

        // query builder select to test Dql later
        $queryBuilder = new QueryBuilder($emMock);
        $queryBuilder->select('t')->from(self::CLASS_NAME, 't');

        // query name generator mock
        $queryNameGeneratorMock = $this->createMock(QueryNameGeneratorInterface::class);

        $filterMock->apply($queryBuilder, $queryNameGeneratorMock, self::CLASS_NAME, 'get', ['filters' => $filters]);
        $this->assertEquals($expectedDQL, $queryBuilder->getDQL());
    }

    private function createFilterWithMocks(bool $discriminator = true): DiscriminatorFilter
    {
        // metadata mock
        $metadataMock = $this->createMock(ClassMetadataInfo::class);
        if ($discriminator) {
            $metadataMock->discriminatorColumn = [
                'name' => 'discriminator',
                'type' => 'string',
            ];
            $metadataMock->discriminatorMap = [
                'test1' => self::CLASS_NAME . '\\Test1',
                'test2' => self::CLASS_NAME . '\\Test2',
            ];
        }

        // entity manager mock
        $emMock = $this->createMock(ObjectManager::class);
        $emMock->method('getClassMetadata')->willReturn($metadataMock);

        // doctrine mock
        $doctrineMock = $this->createMock(ManagerRegistry::class);
        $doctrineMock->method('getManagerForClass')->willReturn($emMock);

        return new DiscriminatorFilter($doctrineMock);
    }
}
