<?php

declare(strict_types=1);

namespace Test\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use Mgo\ApiPlatform\Filter\DiscriminatorFilter;
use PHPUnit\Framework\TestCase;
use Mgo\ApiPlatform\Filter\StrategySearchFilter;
use Symfony\Component\HttpFoundation\RequestStack;
use ApiPlatform\Core\Api\IriConverterInterface;

class StrategySearchFilterTest extends TestCase
{
    public const CLASS_NAME = 'Entity\Class';

    public function applyData(): array
    {
        return [
            'no filter defined in GET parameters (/api)' => [
                'SELECT t FROM Entity\Class t', // expected DQL
                [],
            ],
            'only _stategy "or" set' => [
                'SELECT t FROM Entity\Class t',
                [StrategySearchFilter::STRATEGY_PARAMETER_NAME => StrategySearchFilter::STRATEGY_NAME],
            ],
            '_stategy "or" with unknown parameter' => [
                'SELECT t FROM Entity\Class t',
                [
                    StrategySearchFilter::STRATEGY_PARAMETER_NAME => StrategySearchFilter::STRATEGY_NAME,
                    'unknown' => 'unknown',
                ],
            ],
            '_stategy "or" with testField1' => [
                'SELECT t FROM Entity\Class t WHERE t.testField1 = :testField1_parameter',
                [
                    StrategySearchFilter::STRATEGY_PARAMETER_NAME => StrategySearchFilter::STRATEGY_NAME,
                    'testField1' => '123',
                ],
            ],
            '_stategy "or" with testField2' => [
                'SELECT t FROM Entity\Class t WHERE t.testField2 = :testField2_parameter',
                [
                    StrategySearchFilter::STRATEGY_PARAMETER_NAME => StrategySearchFilter::STRATEGY_NAME,
                    'testField2' => '456',
                ],
            ],
            '_stategy "or" with testField1, testField2' => [
                // phpcs:ignore
                'SELECT t FROM Entity\Class t WHERE t.testField1 = :testField1_parameter OR t.testField2 = :testField2_parameter',
                [
                    StrategySearchFilter::STRATEGY_PARAMETER_NAME => StrategySearchFilter::STRATEGY_NAME,
                    'testField1' => '444',
                    'testField2' => '555',
                ],
            ],
            '_stategy "or" with testField1, testField2 avec exclude testField2' => [
                // phpcs:ignore
                'SELECT t FROM Entity\Class t WHERE t.testField1 = :testField1_parameter AND t.testField2 = :testField2_parameter',
                [
                    StrategySearchFilter::STRATEGY_PARAMETER_NAME => StrategySearchFilter::STRATEGY_NAME,
                    'testField1' => '444',
                    'testField2' => '555',
                    StrategySearchFilter::EXCLUDE_PARAMETER_NAME => 'testField2',
                ],
            ],
            '_stategy "and" with testField1, testField2' => [
                // phpcs:ignore
                'SELECT t FROM Entity\Class t WHERE t.testField1 = :testField1_parameter AND t.testField2 = :testField2_parameter',
                [
                    'testField1' => '666',
                    'testField2' => '777',
                ],
            ],
            '_stategy "or" with testField1, NOT testField2' => [
                // phpcs:ignore
                'SELECT t FROM Entity\Class t WHERE t.testField1 = :testField1_parameter OR t.testField2 != :testField2_parameter',
                [
                    StrategySearchFilter::STRATEGY_PARAMETER_NAME => StrategySearchFilter::STRATEGY_NAME,
                    'testField1' => '777',
                    'testField2' => ['not' => '999'],
                ],
            ],
            '_stategy "and" with NOT testField1, testField2' => [
                // phpcs:ignore
                'SELECT t FROM Entity\Class t WHERE t.testField1 != :testField1_parameter AND t.testField2 = :testField2_parameter',
                [
                    'testField1' => ['not' => '000'],
                    'testField2' => '222',
                ],
            ],
            '_stategy "or" with testField1, NOT testField2 avec exclude testField2' => [
                // phpcs:ignore
                'SELECT t FROM Entity\Class t WHERE t.testField1 = :testField1_parameter AND t.testField2 != :testField2_parameter',
                [
                    StrategySearchFilter::STRATEGY_PARAMETER_NAME => StrategySearchFilter::STRATEGY_NAME,
                    'testField1' => '666',
                    'testField2' => ['not' => '999'],
                    StrategySearchFilter::EXCLUDE_PARAMETER_NAME => 'testField2',
                ],
            ],
            '_stategy "or" with testField1, NOT testField2 as array values' => [
                // phpcs:ignore
                'SELECT t FROM Entity\Class t WHERE t.testField1 = :testField1_parameter OR t.testField2 NOT IN (:testField2_parameter)',
                [
                    StrategySearchFilter::STRATEGY_PARAMETER_NAME => StrategySearchFilter::STRATEGY_NAME,
                    'testField1' => '777',
                    'testField2' => ['not' => ['111', '222']],
                ],
            ],
        ];
    }

    /**
     * @dataProvider applyData
     */
    public function testApply(string $expectedDQL, array $filters = []): void
    {
        $filterMock = $this->createFilterWithMocks();

        // query builder select to test Dql later
        $queryBuilder = new QueryBuilder($this->createMock(EntityManagerInterface::class));
        $queryBuilder->select('t')->from(self::CLASS_NAME, 't');

        // query name generator mock
        $queryNameGeneratorMock = $this->createMock(QueryNameGeneratorInterface::class);
        $queryNameGeneratorMock->method('generateParameterName')->willReturnCallback(function ($field) {
            return "{$field}_parameter";
        });

        $filterMock->apply(
            $queryBuilder,
            $queryNameGeneratorMock,
            self::CLASS_NAME,
            'get',
            [
                'filters' => $filters,
            ]
        );
        $this->assertEquals($expectedDQL, $queryBuilder->getDQL());
    }

    private function createFilterWithMocks(): StrategySearchFilter
    {
        // metadata mock
        $metadataMock = $this->createMock(ClassMetadata::class);
        $metadataMock->fieldMappings = [ // @phpstan-ignore-line
            'testField1' => [
                'fieldName' => 'testField1',
                'type' => 'integer',
                'scale' => 0,
                'length' => null,
                'unique' => true,
                'nullable' => true,
                'precision' => 0,
                'columnName' => 'testField1',
                'declared' => self::CLASS_NAME,
            ],
            'testField2' => [
                'fieldName' => 'testField2',
                'type' => 'string',
                'scale' => 0,
                'length' => null,
                'unique' => true,
                'nullable' => true,
                'precision' => 0,
                'columnName' => 'testField2',
                'declared' => self::CLASS_NAME,
            ],
        ];
        $metadataMock->method('hasField')->willReturnCallback(function ($field) use ($metadataMock) {
            return isset($metadataMock->fieldMappings[$field]);
        });
        $metadataMock->method('hasAssociation')->willReturnCallback(function ($field) use ($metadataMock) {
            return isset($metadataMock->associationMappings[$field]);
        });
        $metadataMock->method('getAssociationTargetClass')->willReturn(self::CLASS_NAME);

        // entity manager mock
        $emMock = $this->createMock(ObjectManager::class);
        $emMock->method('getClassMetadata')->willReturn($metadataMock);

        // doctrine mock
        $doctrineMock = $this->createMock(ManagerRegistry::class);
        $doctrineMock->method('getManagerForClass')->willReturn($emMock);

        // request stack mock
        $requestStackMock = $this->createMock(RequestStack::class);

        // IRI converter mock
        $iriConverterMock = $this->createMock(IriConverterInterface::class);

        return new StrategySearchFilter(
            $doctrineMock,
            $requestStackMock,
            $iriConverterMock,
            null,
            null,
            [
                'testField1' => 'exact',
                'testField2' => 'exact',
            ]
        );
    }
}
